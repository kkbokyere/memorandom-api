/* eslint-disable */
var webpack = require('webpack');

module.exports = {
  entry: ['./entry.js'],
  output: {
    filename: 'dist/bundle.js',
  },
  module: {
    rules: [
      /*
       your other rules for JavaScript transpiling go in here
       */
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
        },
      },
    ],
  },
  plugins: [],
  devtool: 'source-map',
};
