import express from 'express';
import Idea from '../models/idea';

const router = express.Router();

const saveIdea = (req, res) => {
  const newIdea = new Idea();
  newIdea.title = req.body.title;
  newIdea.body = req.body.body;
  newIdea.save((err) => {
    if (err) {
      res.send(err);
    } else {
      res.json({ data: newIdea });
    }
  });
};

const removeIdea = (req, res) => {
  Idea.remove({
    _id: req.params.id,
  }, (err) => {
    if (err) {
      res.send(err);
    }
    res.json({ message: `${req.params.id} deleted` });
  });
};

const updateIdea = (req, res) => {
  Idea.findById({ _id: req.params.id }, (err, idea) => {
    if (err) {
      res.send(err);
    }
    Object.assign(idea, req.body).save((saveErr, updatedIdea) => {
      if (saveErr) {
        res.send(saveErr);
      }
      res.json({ data: updatedIdea });
    });
  });
};

const getIdea = (req, res) => {
  Idea.findById(req.params.id, (err, idea) => {
    if (err) {
      res.send(err);
    }
    res.json({ data: idea });
  });
};

const getIdeas = (req, res) => {
  Idea.find((err, ideas) => {
    if (err) {
      res.send(err);
    }
    return res.json({ data: ideas });
  });
};

// middleware to use for all requests
router.use((req, res, next) => {
  // Enable Cross Origin Resource sharing to allow other apps to make calls to mongodb port
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  console.log('Memorandom');
  next(); // make sure we go to the next routes and don't stop here
});

/**
 * GET /ideas route to retrieve all ideas
 */
router.route('/ideas')
  .get(getIdeas);

/**
 * POST /ideas/new route to create new ideas
 */
router.route('/ideas/new')
  .post(saveIdea);

/**
 * GET individual idea
 */
router.route('/ideas/:id')
  .get(getIdea);

/**
 * PUT/Update idea
 */
router.route('/idea/update/:id')
  .put(updateIdea);

/**
 * PUT/Update idea
 */
router.route('/idea/delete/:id')
  .delete(removeIdea);

router.use('/ideas', router);

export default router;
