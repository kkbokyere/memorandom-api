import express from 'express';
import mongoose from 'mongoose'; // Provides MongoDB data modelling schema, validation
import bodyParser from 'body-parser'; // Allows us to accept POST params from browser
import routes from '../src/routes/ideas';

let config = require('../config/development.json');

if (process.env.NODE_ENV === 'test') {
  config = require('../config/test.json');
}

const app = express();

const port = config.server.port;

// Connect to Mongoose
mongoose.connect(`mongodb://${config.database.host}:${config.database.port}/${config.database.name}`);

const db = mongoose.connection;

// Entry Route
app.get('/', (req, res) => {
  res.send('You made it!! Use /api/v1/');
});

// Deal with the response before it gets to the client.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json' }));

// all of our routes will be prefixed with /api
app.use('/api/v1', routes);

app.listen(port);

// for testing
export default app;
