const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Idea Schema
const IdeaSchema = new Schema({
  title: {
    type: String,
  },
  body: {
    type: String,
    maxLength: 140,
  },
  created_date: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

export default mongoose.model('Idea', IdeaSchema);
