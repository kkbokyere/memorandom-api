# Memorandom Microservice API.

A simple API microservice used power the Memorandom web app.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Currently, no live deployment pipeline has been catered for.

### Prerequisites

In order to get this working, you would need NodeJS and MongoDB installed on your dev machine. (You could use Yarn too is you prefer)

For Node installation instructions go here: https://nodejs.org/en/download/

For Mongo DB installation instructions go here: https://docs.mongodb.com/manual/tutorial/install-mongodb-enterprise-on-os-x/

Once you have these installed and you have checked out this repo, you should be ready to start installation.

### Installing

Check to make sure you have Node, NPM and MongoDB.

```
$ node -v
$ npm -v
$ mongodb -v
```

Start your MongoDB server on Port 3000

```
$ mongod --port 3000
```

Run the install script

```
$ npm install
```

Assuming all dependencies downloaded okay, run the start:dev script and the service should start working!

```
$ npm run start:dev
```

If its all good, check http://localhost:8080/api/v1/ideas

## Running the tests

Test are built using [Mocha](http://mochajs.org/). you can run them by using the following command:

```
$ npm run test:dev
```

## Deployment

If it were to be deployed to live, It would use Docker to create a container on the deployment server.

## Built With

* [NodeJS](https://nodejs.org/en/) - Server framework
* [ExpressJS](https://expressjs.com/) - HTTP Middleware
* [MongoDB](https://www.mongodb.com/) - NoSQL Database
* [Mongoose](http://mongoosejs.com/) - MongoDB modelling schema
* [Mocha](http://mochajs.org/) - Test framework

### Improvements

- Schema documentation using maybe Swagger 
- Better test coverage
- Use a mock db instance to test instead of having to create another local test db
- Typescript for stricter type syntax