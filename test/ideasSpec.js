import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../src/server';
import mongoose from 'mongoose';
import Idea from '../src/models/idea';

const expect = chai.expect;
const should = chai.should();

chai.use(chaiHttp);

describe('Ideas', () => {
  beforeEach((done) => { // Before each test we empty the database
    Idea.remove({}, () => {
      done();
    });
  });
  /*
   * Test the /GET route
   */
  describe('/GET ideas', () => {
    it('it should GET all the ideas', (done) => {
      chai.request(server)
        .get('/api/v1/ideas')
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.data).to.be.a('array');
          expect(res.body.data).to.lengthOf(0);
          done();
        });
    });
  });
  /*
   * Test the /POST route
   */
  describe('/POST ideas/new/', () => {
    it('it should POST a new idea with valid fields', (done) => {
      const newIdeaTest = {title: 'test title', body: 'test body'};
      chai.request(server)
        .post('/api/v1/ideas/new')
        .send(new Idea(newIdeaTest))
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('object');
          expect(res.body).to.have.property('data');
          expect(res.body.data).to.have.property('title').to.equal(newIdeaTest.title);
          expect(res.body.data).to.have.property('body').to.equal(newIdeaTest.body);
          // res.body.data.should.have.property('created_date').eql(new Date().toISOString());
          done();
        });
    });

    it('it should POST a new idea with a field missing', (done) => {
      const newIdeaTest = {title: 'test title 2'};
      chai.request(server)
        .post('/api/v1/ideas/new')
        .send(new Idea(newIdeaTest))
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('object');
          expect(res.body).to.have.property('data');
          expect(res.body.data).to.have.property('title').to.equal(newIdeaTest.title);
          expect(res.body.data).not.to.have.property('body');
          expect(res.body.data).to.have.property('created_date');
          done();
        });
    });
  });
  /*
   * Test the /GET route
   */
  describe('/GET ideas/:id idea', () => {
    it('it should GET a idea by the given id', (done) => {
      let newIdea = new Idea({title: 'the king', body: 'new body'});
      newIdea.save((err, idea) => {
        chai.request(server)
          .get('/api/v1/ideas/' + idea.id)
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body.data).to.be.a('object');
            expect(res.body.data).to.have.property('_id').eql(idea.id);
            done();
          });
      });
    });
  });
  /*
   * Test the /UPDATE route
   */
  describe('/PUT idea/update/:id', () => {
    it('it should UPDATE', (done) => {
      const newIdea = new Idea({ title: 'new idea', body: 'original idea body' });
      const updatedIdea = { title: 'update idea', body: 'updated original idea body' };
      newIdea.save((err, idea) => {
        chai.request(server)
          .put(`/api/v1/idea/update/${idea.id}`)
          .send(updatedIdea)
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body.data).to.be.a('object');
            expect(res.body.data).to.have.property('_id').eql(idea.id);
            expect(res.body.data).to.have.property('title').eql(updatedIdea.title);
            done();
          });
      });
    });
  });
  /*
   * Test the /DELETE route
   */
  describe('/DELETE idea/delete/:id', () => {
    it('it should DELETE', (done) => {
      const newIdea = new Idea({ title: 'new idea to delete', body: 'original idea body to delete' });
      newIdea.save((err, idea) => {
        chai.request(server)
          .delete(`/api/v1/idea/delete/${idea.id}`)
          .end((err, res) => {
          console.log(res.body.data);
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('message')
              .to.equal(`${idea.id} deleted`);
            done();
          });
      });
    });
  });
});
